import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomepageComponent } from './components/views/homepage/homepage.component';
import { GuestmenuComponent } from './components/menus/guestmenu/guestmenu.component';
import { SignupComponent } from './components/views/signup/signup.component';
import { SignupformComponent } from './components/forms/signupform/signupform.component';
import { SigninComponent } from './components/views/signin/signin.component';
import { SigninformComponent } from './components/forms/signinform/signinform.component';
import { UsermenuComponent } from './components/menus/usermenu/usermenu.component';
import { HomeuserComponent } from './components/views/homeuser/homeuser.component';
import { GamecardComponent } from './components/cards/gamecard/gamecard.component';
import { GameinvitebtnComponent } from './components/buttons/gameinvitebtn/gameinvitebtn.component';
import { MatesearchformComponent } from './components/forms/matesearchform/matesearchform.component';
import { GamesearchformComponent } from './components/forms/gamesearchform/gamesearchform.component';
import { ProfileuserComponent } from './components/views/profileuser/profileuser.component';
import { SearchmateComponent } from './components/views/searchmate/searchmate.component';
import { SearchgameComponent } from './components/views/searchgame/searchgame.component';
import { AdvancedGameSearchFormComponent } from './components/forms/advanced-game-search-form/advanced-game-search-form.component';
import { AdvancedMateSearchFormComponent } from './components/forms/advanced-mate-search-form/advanced-mate-search-form.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    GuestmenuComponent,
    SignupComponent,
    SignupformComponent,
    SigninComponent,
    SigninformComponent,
    UsermenuComponent,
    HomeuserComponent,
    GamecardComponent,
    GameinvitebtnComponent,
    MatesearchformComponent,
    GamesearchformComponent,
    ProfileuserComponent,
    SearchmateComponent,
    SearchgameComponent,
    AdvancedGameSearchFormComponent,
    AdvancedMateSearchFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
