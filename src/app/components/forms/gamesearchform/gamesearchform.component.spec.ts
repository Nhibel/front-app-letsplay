import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GamesearchformComponent } from './gamesearchform.component';

describe('GamesearchformComponent', () => {
  let component: GamesearchformComponent;
  let fixture: ComponentFixture<GamesearchformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GamesearchformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GamesearchformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
