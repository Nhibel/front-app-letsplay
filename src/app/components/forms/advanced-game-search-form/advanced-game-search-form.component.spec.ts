import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedGameSearchFormComponent } from './advanced-game-search-form.component';

describe('AdvancedGameSearchFormComponent', () => {
  let component: AdvancedGameSearchFormComponent;
  let fixture: ComponentFixture<AdvancedGameSearchFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvancedGameSearchFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedGameSearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
