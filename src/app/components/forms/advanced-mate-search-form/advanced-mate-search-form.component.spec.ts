import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedMateSearchFormComponent } from './advanced-mate-search-form.component';

describe('AdvancedMateSearchFormComponent', () => {
  let component: AdvancedMateSearchFormComponent;
  let fixture: ComponentFixture<AdvancedMateSearchFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvancedMateSearchFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedMateSearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
