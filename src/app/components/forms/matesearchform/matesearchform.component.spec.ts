import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatesearchformComponent } from './matesearchform.component';

describe('MatesearchformComponent', () => {
  let component: MatesearchformComponent;
  let fixture: ComponentFixture<MatesearchformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatesearchformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatesearchformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
