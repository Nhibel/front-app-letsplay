import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameinvitebtnComponent } from './gameinvitebtn.component';

describe('GameinvitebtnComponent', () => {
  let component: GameinvitebtnComponent;
  let fixture: ComponentFixture<GameinvitebtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameinvitebtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameinvitebtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
