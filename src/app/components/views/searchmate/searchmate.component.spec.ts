import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchmateComponent } from './searchmate.component';

describe('SearchmateComponent', () => {
  let component: SearchmateComponent;
  let fixture: ComponentFixture<SearchmateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchmateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchmateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
