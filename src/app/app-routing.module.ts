import { SearchgameComponent } from './components/views/searchgame/searchgame.component';
import { SearchmateComponent } from './components/views/searchmate/searchmate.component';
import { ProfileuserComponent } from './components/views/profileuser/profileuser.component';
import { HomeuserComponent } from './components/views/homeuser/homeuser.component';
import { SigninComponent } from './components/views/signin/signin.component';
import { SignupComponent } from './components/views/signup/signup.component';
import { HomepageComponent } from './components/views/homepage/homepage.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/home' },
  { path: 'home', component: HomepageComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'signin', component: SigninComponent },
  { path: 'homeuser', component: HomeuserComponent },
  { path: 'profile', component: ProfileuserComponent },
  { path: 'searchmate', component: SearchmateComponent },
  { path: 'searchgame', component: SearchgameComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
